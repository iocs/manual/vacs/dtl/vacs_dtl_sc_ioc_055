# IOC for DTL-050 vacuum ion pumps

## Used modules

*   [vac_ctrl_digitelqpce](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_digitelqpce)


## Controlled devices

*   DTL-050:Vac-VEPI-10001
    *   DTL-050:Vac-VPN-10000
    *   DTL-050:Vac-VPN-20000
    *   DTL-050:Vac-VPN-30000
    *   DTL-050:Vac-VPN-40000
